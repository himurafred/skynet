/******** get  data **********/

var inputs = readline().split(' ');
var N = parseInt(inputs[0]); // the total number of nodes in the level, including the gateways
var L = parseInt(inputs[1]); // the number of links
var E = parseInt(inputs[2]); // the number of exit gateways


/******************/

var skynet = (function(){

    if (!Array.prototype.remove) {
        Array.prototype.remove = function(val) {
        var i = this.indexOf(val);
            return i>-1 ? this.splice(i, 1) : [];
        };
    }

    var self = {};
    var links = [];
    var gateways = [];

    var fillLinks = function fillLinks(L){
        for (var i = 0; i < L; i++) {
            var inputs = readline().split(' ');
            var N1 = parseInt(inputs[0]); // N1 and N2 defines a link between these nodes
            var N2 = parseInt(inputs[1]);

            //Create link 
            links.push(N1 + ' ' + N2);
        }
    };

    var fillGateway = function fillGateway(E){
      for (var i = 0; i < E; i++) {
          var EI = parseInt(readline()); // the index of a gateway node
          gateways.push(EI);
      }
    };

    var fillLinksforGateway = function fillLinksforGateway(){
        var linksOnlyForGateway = [];
        
        for(var i = 0, linklength = links.length; i < linklength; i++){
            for(var j =0, gatewayslength = gateways.length; j < gatewayslength; j++){
                var pointLink = links[i].split(' ');

                if((parseInt(pointLink[0]) === gateways[j] || parseInt(pointLink[1]) === gateways[j]) 
                    && linksOnlyForGateway.indexOf(links[i]) === -1){  
                   linksOnlyForGateway.push(links[i]);
                }
            }
        }

        links = linksOnlyForGateway;
    };
 
    self.displayLink = function displayLink(){
      for (var i = 0, length = links.length; i < length; i++){
          printErr('link just for gateways', links[i]);  
      }
    };

    self.blockVirus = function blockVirus(SI){      
        var linkIndice = 0;
        for(var i = 0, linkslength = links.length; i < linkslength; i++){
            //Check if the virus is on the node next one gateways
            var pointLink = links[i].split(' ');

            if(parseInt(pointLink[0]) === SI || parseInt(pointLink[1]) === SI){
                linkIndice = i;
                break;
            }
        }        

        //close the link where is SI if link Indice != 0, evenf if close the first link     
        print(links[linkIndice]);

        //task clean 
        links.remove(links[linkIndice]);
    };

    self.init = function(L, E){ 
        fillLinks(L);
        fillGateway(E);
        fillLinksforGateway();
    };

    return self;
})();

skynet.init(L, E);
skynet.displayLink();

// game loop
while (true) {
    var SI = parseInt(readline()); // The index of the node on which the Skynet agent is positioned this turn
    skynet.blockVirus(SI);
}