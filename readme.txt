init projet skynet

- methode de commit 
	- création de la branche master 
		- création de la branche developpement
			- création d'une branche par feature -> 1 feature = 1 test d'acceptance (feature-test-X)
			- commit des modifications dans la branche feature
			- fin de développement de la feature 
			- récupération de la branche de développement et update
			- merge de la branche feature dans la branche developpement
			- push sur la branche développement
			- suppression de la branche feature 
		- fin de developpement des features 
			- creation de la branche release 
			- merge de la branche de release vers master
			- merge de la branche de release vers développement
			- tags master

- description des algorithmes :

	test 1 : fermeture du lien relié à la gateway
	test 2 : fermeture des liens reliés à la gateway
	test 3 : fermeture des liens du virus avec en priorité fermture du lien si relié à la gateway
	test 4 : fermeture des liens du virus avec en priorité fermture des liens reliés aux gateways
	version optimisée : fermture en priorité des liens reliés au gateway, et si le virus arrive à proximité
						d'une gateways,fermeture du lien entre le virus et la gateway

	optimisation possible mais non nécessaire au vue du dernier graphe :
		-	fermeture des liens relié à la gateway la plus proche du virus en calculant le parcours optimale du virus via le parcours de graphe (en utilisant l'algorithme du voyageur de commerce) 
